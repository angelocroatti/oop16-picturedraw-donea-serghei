package view;

public interface ShowTheGUIView {

    /**
     * Mostra il frame.
     */
    void start();

}