package controller;

public enum DrawTool {

    DRAW_TOOL, TEXT_TOOL, BUCKET_TOOL, BRUSH
}
